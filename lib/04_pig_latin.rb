require 'byebug'

def translate(string)
  strings = string.split(' ')
  vowels = ['a', 'e', 'i', 'o', 'u']
  result = []
  strings.each do |word|
    index = 0
    word.split("").each_with_index do |ch, i|
      if vowels.include?(ch) && ch != 'u'
        index = i
        break
      end
    end
    result << word[index..-1] + word[0...index] + "ay"
  end
  result.join(' ')
end
