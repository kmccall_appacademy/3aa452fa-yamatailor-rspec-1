def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(array)
  array.reduce(0) { |sum, value| sum + value }
end

def multiply(array)
  array.reduce(:*)
end

def power(number, n)
  number**n
end

def factorial(n)
  n > 0 ? (1..n).reduce(:*) : 0
end
