def echo(echoing)
  echoing
end

def shout(shouting)
  shouting.upcase
end

def repeat(repeating, times=2)
  ("#{repeating} " * times).strip
end

def start_of_word(word, index)
  word.slice(0...index)
end

def first_word(string)
  string.split[0]
end

def titleize(word)
  little_words = ['and', 'over', 'the']
  words = word.split(' ')
  capitalized = words.map.with_index do |word, index|
    if word == words[0] && index == 0
      word.capitalize
    elsif !little_words.include?(word)
      word.capitalize
    else
      word
    end
  end
  capitalized.join(' ')
end
